import os
import sys
import json

all = (
    'ArnoldRenderer',
)


def replace_tokens(token_dict, line):
    out_str = line
    for key, val in token_dict.iteritems():
        out_str = out_str.replace("%"+key+"%", val)
    return out_str


class ArnoldRenderer(object):

    @staticmethod
    def create_test(parsed_args, test_config):
        renderer = parsed_args.renderer
        test_name = parsed_args.test_name

        TEST_HOME = os.path.join('tests', renderer, test_name)
        TEST_IMGS = os.path.join(TEST_HOME,'img')
        output_lines = []

        for filename in test_config['templates'][parsed_args.template]:
            fin = open(os.path.join('renderers', parsed_args.renderer, filename), 'r')
            lines = fin.readlines()

            # create the test directory
            try:
                os.makedirs(TEST_HOME)
            except Exception, e:
                pass
            # create the test directory
            try:
                os.makedirs(TEST_IMGS)
            except Exception, e:
                pass

            output_lines.append(("#"*30)+"\n")
            output_lines.append("# " + filename + " BEGIN\n")
            for line in lines:
                output_lines.append(
                    replace_tokens({
                        'TEST_HOME': TEST_HOME,
                        'TEST_NAME': test_name,
                        'TEST_CASE': parsed_args.test_case
                        },
                        line
                    )
                )
            output_lines.append("# " + filename + " END \n")
            output_lines.append(("#"*30)+"\n\n")
        fout = open(os.path.join(
            TEST_HOME,
            "%s_%s" % (parsed_args.test_case, test_config['test_file']),
            ),
            'w'
        )
        fout.write("".join(output_lines))
        fout.close

    @staticmethod
    def get_run_cmd(test_config):
        cmd = test_config['cmd']
        for path in test_config['shader_paths']:
            cmd += " -l %s" % path
        return cmd

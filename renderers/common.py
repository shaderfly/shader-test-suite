import os
import importlib
import json
from misc.singleton import Singleton


class constants(Singleton):
    CMDS_HOME = "cmds"
    RENDERERS_HOME = "renderers"
    RESULTS_HOME = 'results'
    CONFIG_FILENAME = "config.json"
    TESTS_HOME = "tests"

    @staticmethod
    def get_test_path(renderer):
        config = load_renderer_config(renderer)
        return config.get('tests_path', constants.TESTS_HOME)


def _load_json(config_file):
    if os.path.exists(config_file):
        # print "Loading config file %s" % config_file
        fin = open(config_file, 'r')
        config = json.load(fin)
        fin.close()
        return config


def get_class_name(parsed_args):
    return parsed_args.renderer[0].upper() + parsed_args.renderer[1:].lower()+'Renderer'


def get_all_renderers():
    all_renderers = next(os.walk(constants.RENDERERS_HOME))[1]
    renderers = {}
    for renderer in all_renderers:
        module_name = constants.RENDERERS_HOME+'.'+renderer
        renderers[renderer] = importlib.import_module(module_name)
    return renderers


def get_all_tests(renderer):
    tests_root = os.path.join(constants.get_test_path(renderer), renderer)
    all_tests = next(os.walk(tests_root))[1]
    return all_tests


def get_all_test_cases(renderer, test, config):
    tests_root = os.path.join(constants.get_test_path(renderer), renderer, test)
    all_test_cases = next(os.walk(tests_root))[2]
    output_test_cases = []
    for test_case in all_test_cases:
        suffix = "_" + config['test_file']
        o_test_case = test_case[:-len(suffix)]
        output_test_cases.append(o_test_case)
    return output_test_cases


def load_renderer_config(renderer):
    '''Load the config for the testing suite in the following order
    1 - From the renderer_path,
    2 - From the root of the renderers package
    '''

    conf = dict()
    # start at the renderer_path and walk upwards ot the root of the applicaton.
    config_file = os.path.join(
        constants.RENDERERS_HOME,
        renderer,
        constants.CONFIG_FILENAME
    )
    renderer_config = _load_json(config_file)
    if renderer_config:
        conf.update(renderer_config)
    # Load the renderer root
    config_file = os.path.join(
        constants.RENDERERS_HOME,
        constants.CONFIG_FILENAME
    )
    common_config = _load_json(config_file)
    if common_config:
        conf.update(common_config)

    return conf


def load_test_config(renderer, test_name):
    '''Load the config for the test'''
    # if test_name is not none, then load the config from that test
    config_file = os.path.join(
        constants.get_test_path(renderer),
        renderer,
        test_name,
        constants.CONFIG_FILENAME)
    return _load_json(config_file)

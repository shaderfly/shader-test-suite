ShaderFly Testing Suite
===

This is a command line tool for the creation and management of unit tests for shaders. For more information go to http://shaderfly.com/testing-suite/

Installation
===
This application has some requirements which must be installed in order for the application to run properly. The dependencies for this application are:

* python 2.7
* [virtualenv](https://virtualenv.pypa.io/en/stable/)
* [pip](https://pypi.python.org/pypi/pip)
* [node.js](https://nodejs.org/en/)
* [openImageIO](https://sites.google.com/site/openimageio/home)


Please follow these steps to install any necessary dependencies

1. Create a python virtual Environment by typing **virtualenv env** at the root of the project
2. Type **source env/bin/activate** in order to initiate the virtual environment
3. Type **pip install -r requirements.txt** to install all python requirements
4. cd into **html**
5. Type **npm install** to install html dependencies

Current State
===
The application is still very young. So far only the Arnold rendering engine is supported and there are still a lot of feature to implement. At the moment this is still a minimum viable product.

Usage
===
The testing tool provides the following commands; create, list, run.


Create
---
This command allows you to create new unit test. Tests are grouped by test name, which is usually the shader that you are trying to test. Each test can have several test cases associated with it. A test case is used to test an specific feature of the shader.

    create [-h] [-t TEMPLATE] test_name test_case

    positional arguments:
      test_name             The name of the test
      test_case             The name of the test case

    optional arguments:
      -h, --help            show this help message and exit
      -t TEMPLATE, --template TEMPLATE
                            the template to use for the test

List
---
This command allows you to inspect the current registered tests in the system and the templates that have been installed for a given renderer.

    list [-h] [-i {tests,templates}]

    optional arguments:
      -h, --help            show this help message and exit
      -i {tests,templates}, --item {tests,templates}
                            the type item to list

Run
---
This command runs the unit tests that have been created in the system. The unit tests are usually small scenes which after rendering, are compared with the previously rendered version of the same scene. If the difference between the two images is higher than the requested threshold, the test will fail.

    run [-h] [-t [TEST [TEST ...]]]

    optional arguments:
      -h, --help            show this help message and exit
      -t [TEST [TEST ...]], --test [TEST [TEST ...]]
                            the name of the tests to run, defaults to all

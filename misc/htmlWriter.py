import os
import shutil
import glob
from jinja2 import Environment, select_autoescape, FileSystemLoader


class HtmlWriter(object):

    def __init__(self, renderer):
        self.renderer = renderer
        self.env = Environment(
            loader=FileSystemLoader('html/templates/'),
            autoescape=select_autoescape(['html', 'xml'])
        )

    def _get_log_content(self, results_dir, test_case, log_file):

        render_output_filename = os.path.join(
            results_dir,
            "%s_%s_output.txt" % (self.renderer, test_case)
        )
        if not os.path.exists(render_output_filename):
            return ""

        fin = open(log_file, 'r')
        log_output = fin.read()
        fin.close()
        return log_output

    def _copy_test_images(self, html_results_dir, image_dir, test, test_case, rendered_img):
        image_filename = "%s_%s_%s.exr" % (test, test_case, rendered_img)
        image_html_path = os.path.join(
            'img',
            image_filename.replace(".exr", ".png")
        )
        image_dst_filename = os.path.join(
            html_results_dir,
            image_html_path
        )
        image_src_filename = os.path.join(
            image_dir,
            image_filename
        )
        # create the folder if necessary
        try:
            os.mkdir(os.path.join(html_results_dir, 'img'))
        except:
            pass
        # Conver the image using oiiotool
        cp_cmd = "oiiotool %s -o %s" % (
            image_src_filename,
            image_dst_filename
        )
        os.system(cp_cmd)
        return image_html_path

    def generate(self, config):
        import renderers.common as rend_common
        # loop through all the tests for the given renderer
        all_tests = rend_common.get_all_tests(self.renderer)
        # for each test, gather the render output and the diff output
        template = self.env.get_template('master_layout.html')
        html_results_dir = os.path.join(
            rend_common.constants.get_test_path(self.renderer),
            rend_common.constants.RESULTS_HOME,
        )
        context = {'tests':{}}
        for test in all_tests:
            test_cases = rend_common.get_all_test_cases(self.renderer, test, config)
            results_dir = os.path.join(
                rend_common.constants.get_test_path(self.renderer),
                self.renderer,
                test,
                rend_common.constants.RESULTS_HOME,
            )
            context['tests'][test] = {}
            context['tests'][test]['cases'] = []
            for test_case in test_cases:
                test_dict = {}
                context['tests'][test]['cases'].append(test_dict)
                image_dir = os.path.join(
                    rend_common.constants.get_test_path(self.renderer),
                    self.renderer,
                    test,
                    'img',
                )
                # Read the render output
                render_output_filename = os.path.join(
                    results_dir,
                    "%s_%s_output.txt" % (self.renderer, test_case)
                )
                test_dict['name'] = test_case
                test_dict['render_output'] = self._get_log_content(
                    results_dir,
                    test_case,
                    render_output_filename
                )
                # read the idiff output
                idiff_output_filename = os.path.join(
                    results_dir,
                    "idiff_%s_output.txt" % test_case
                )
                test_dict['idif_output'] = self._get_log_content(
                    results_dir,
                    test_case,
                    idiff_output_filename
                )
                # set the status for the test. It will always be the last line
                # if the image diff
                test_dict['status'] = test_dict['idif_output'].split()[-1]

                # convert the current image from ext to png and move it to the
                # results folder
                test_dict['current_image_filename'] = self._copy_test_images(
                    html_results_dir,
                    image_dir,
                    test,
                    test_case,
                    'current'
                )
                test_dict['output_image_filename'] = self._copy_test_images(
                    html_results_dir,
                    image_dir,
                    test,
                    test_case,
                    'output'
                )

        # Write index.html output
        try:
            os.mkdir(html_results_dir)
        except:
            pass

        template_output = template.render(**context)
        results_output_filename = os.path.join(html_results_dir, "index.html")
        fout = open(results_output_filename, 'w')
        fout.write(template_output)
        fout.close()

        # copy any necessary webdev files
        cwd = os.getcwd()
        os.chdir(html_results_dir)
        source = os.path.join('..', '..', 'html', 'node_modules')
        try:
            os.symlink(
                source,
                'node_modules'
            )
        except:
            pass

        img_path = os.path.join('..', '..', 'html', 'img', '*')
        imgs = glob.glob(img_path)
        for _img in imgs:
            try:
                target = os.path.join('img', os.path.basename(_img))
                shutil.copyfile(
                    _img,
                    target
                )
            except Exception, e:
                pass

        os.chdir(cwd)

import sys
import os
import argparse

# import cmds
import importlib

parser = argparse.ArgumentParser(description='Shader unit testing suite.')
parser.add_argument(
    '-r',
    '--renderer', type=str,
    default='arnold',
    help='the type of renderer to use for operations',
    choices=('arnold',)
)
subparsers = parser.add_subparsers(help='sub-command help')


def load_subcommands():
    all_cmds = next(os.walk('cmds'))[1]
    cmds = {}
    for cmd in all_cmds:
        cmds[cmd] = importlib.import_module('cmds.'+cmd+'.cmd')
        cmds[cmd].load(subparsers)

    return cmds


if __name__ == '__main__':
    sub_commands = load_subcommands()
    parsed_args = parser.parse_args(sys.argv[1:])
    sub_commands[sys.argv[1]].run(parsed_args)

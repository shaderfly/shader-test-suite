import os
import json

import renderers.common as rend_common


# create the parser for the "a" command
def load(subparsers):
    list_parser = subparsers.add_parser('list', help='lists the unit test')
    list_parser.add_argument(
        '-i',
        '--item',
        type=str,
        default='tests',
        help='the type item to list',
        choices=('tests', 'templates')
    )


def run(parsed_args):
    if parsed_args.item == 'tests':
        all_cmds = next(os.walk(
            os.path.join(
                rend_common.constants.get_test_path(parsed_args.renderer),
                parsed_args.renderer
            )
        ))[1]
        print "\nMathing tests for renderer %s\n" % parsed_args.renderer
        for cmd in all_cmds:
            print "  "+cmd
    elif parsed_args.item == 'templates':
        fin = open(os.path.join(
            rend_common.constants.RENDERERS_HOME,
            parsed_args.renderer,
            'config.json'
        ), 'r')
        test_config = json.load(fin)
        fin.close()

        print "\nMathing templates for renderer %s\n" % parsed_args.renderer
        for template in test_config['templates'].keys():
            print "  "+template

    print ""

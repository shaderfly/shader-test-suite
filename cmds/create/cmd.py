import os
import json

import renderers.common as rend_common


# create the parser for the "a" command
def load(subparsers):
    create_parser = subparsers.add_parser('create', help='creates a new unit test')
    create_parser.add_argument(
        '-t',
        '--template', type=str,
        help='the template to use for the test',
    )
    create_parser.add_argument(
        'test_name',
        type=str,
        help='The name of the test'
    )
    create_parser.add_argument(
        'test_case',
        type=str,
        help='The name of the test case'
    )


def run(parsed_args):
    config = rend_common.load_renderer_config(parsed_args.renderer)

    # get the template
    if not parsed_args.template:
        parsed_args.template = config['templates'].keys()[0]

    class_name = rend_common.get_class_name(parsed_args)
    renderers = rend_common.get_all_renderers()

    print "\nCreating %s test %s with template %s" % (
        parsed_args.renderer,
        parsed_args.test_name,
        parsed_args.template
    )
    renderers[parsed_args.renderer].__dict__[class_name].create_test(
        parsed_args,
        config
    )

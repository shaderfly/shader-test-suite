import os
import sys
import json
import glob
import subprocess
import time
import shutil

import renderers.common as rend_common
from misc.htmlWriter import HtmlWriter


class Timer:
    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start


# create the parser for the "a" command
def load(subparsers):
    run_parser = subparsers.add_parser('run', help='run the unit test')
    run_parser.add_argument(
        '-t',
        '--test',
        type=str,
        nargs='*',
        default=['all'],
        help='the name of the tests to run, defaults to all',
    )


def compare_images(test, test_case, config):
    results_dir = os.path.join(rend_common.constants.RESULTS_HOME)
    try:
        os.mkdir(results_dir)
    except:
        pass

    output_img = os.path.join('img', "%s_%s_output.exr" % (test, test_case))
    current_img = os.path.join('img',"%s_%s_current.exr" % (test, test_case))

    # if there is no CURRENT image, then we just copy the file and
    # mark the test as success
    if not os.path.exists(current_img):
        shutil.copyfile(output_img, current_img)
        return 'PASS'

    # create the command to compare the pictures
    cmd = config.get('diff_cmd')
    cmd = cmd.replace('%FAIL_THRESHOLD%', str(config.get('fail_threshold')))
    cmd = cmd.replace('%WARN_THRESHOLD%', str(config.get('warn_threshold')))
    cmd += " %s %s" %(current_img, output_img)

    fout = open(os.path.join(results_dir, 'idiff_%s_output.txt' % test_case), 'w')
    proc = subprocess.Popen(
        cmd,
        shell=True,
        stdout=fout,
        stderr=sys.stderr
    )
    rc = proc.wait()
    fout.close()
    if rc == 0:
        return 'PASS'

    return 'FAIL'


def run(parsed_args):
    renderers = rend_common.get_all_renderers()
    class_name = rend_common.get_class_name(parsed_args)

    TESTS_HOME = os.path.abspath(os.path.join(rend_common.constants.TESTS_HOME, parsed_args.renderer))
    RENDERER_HOME = os.path.abspath(os.path.join(rend_common.constants.RENDERERS_HOME, parsed_args.renderer))

    config = rend_common.load_renderer_config(parsed_args.renderer)

    all_tests = next(os.walk(TESTS_HOME))[1]
    if parsed_args.test == ['all']:
        test_list = all_tests
    else:
        test_list = list(set(all_tests) & set(parsed_args.test))

    cwd = os.getcwd()
    results_dir = os.path.join(rend_common.constants.RESULTS_HOME)
    print ""
    for test in test_list:
        test_root = os.path.join(TESTS_HOME, test)
        test_config = rend_common.load_test_config(parsed_args.renderer, test)
        if test_config:
            config.update(test_config)

        # cd into the test dir
        os.chdir(test_root)

        # get all of the test cases
        render_files = glob.glob("*_" + config['test_file'])
        for _the_file in render_files:
            test_case = _the_file.split(config['test_file'])[0][:-1]
            # 1 - Render the files
            sys.stdout.write("Testing %s with test case %s .." % (test, _the_file))
            render_log = os.path.join(
                results_dir,
                '%s_%s_output.txt' % (parsed_args.renderer, test_case)
            )
            try:
                os.mkdir(os.path.dirname(render_log))
            except:
                pass
            fout = open(render_log, 'w')
            cmd = renderers[parsed_args.renderer].__dict__[class_name].get_run_cmd(config)
            cmd += " %s" % _the_file

            with Timer() as t:
                proc = subprocess.Popen(
                    cmd,
                    shell=True,
                    stdout=fout,
                    stderr=sys.stderr
                )
            rc = proc.wait()
            fout.close()
            # 2 - test the image output
            status = compare_images(test, test_case, config)
            sys.stdout.write(".. %s %.04f seconds\n" % (status, t.interval))

    os.chdir(cwd)
    print "\nGenerating reports\n"
    writer = HtmlWriter(parsed_args.renderer)
    writer.generate(config)

    print ""
